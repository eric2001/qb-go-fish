# QB Go Fish
![QB Go Fish](./Screenshots/Go%20Fish%20GUI%20Version.jpg) 

## Description
This is the card game "Go Fish", written in QBasic.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
Run the program.  The contents of your hand will be displayed on the screen.  Enter a card that's in your hand into the prompt to ask your opponent for that card.  The goal is to get 4 copies of each card.  The player with the most sets of 4 wins.

## History
(Dates are estimates based on File Creation and Last Modified dates and may be inaccurate)

This project was started on 03 January 1999.

This project was last updated on 22 January 2001.

Download: [Binary](./bin/Go%20Fish%20GUI%20Final.zip) | [Source](./bin/FISH1Source.zip)
